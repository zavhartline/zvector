<p align="center"><img src="https://cdn.discordapp.com/attachments/803366697699901462/867185495946690601/output.gif" width=400></p>

# ZVector

This project was made originally to replicate the Mad Mew Mew arena transition programmatically. I realized quickly that there wasn't really a terrific way to do vector graphics, so this library was created to allow that.

## Setup Instructions:

**1.)** Place the ZVector folder in the Lua/Libraries folder. Feel free to place ZVector or its contents in whatever file structure desired as long as the two scripts (Polygon.lua and MatrixUtils.lua) are together.

**2.)** Place the ZVectorSprites folder in Sprites

**3.)** At the top of the file you want to use the library with, run 
```lua
local library = require 'Libraries/ZVector/Polygon'
```
If you decided to structure the project differently, alter the path to your preferred location for Polygon.lua

---

## Basic Usage Instructions:

There are two ways to create a shape with this library. One creates an arbitrary shape based off a table of {x,y} tuples. The other creates a regular polygon based off your specifications.

The centerX and centerY parameters are both in absolute terms relative to bottom left of the window.

Arbitrary Polygon:
```lua
library.CreateComplex(centerX, centerY, vertexList, stroke = 5, color32 = {255,255,255})
```

Regular Polygon:
```lua
library.CreateRegular(centerX, centerY, nSides, radius, rotation = 0, stroke = 5, color32 = {255,255,255})
```

Both of these will return a new instance of a polygon. Each time you change a geometric aspect of a polygon, it does not inherently redraw itself. Redrawing may be invoked by `instance.Update()`. If no geometric change has occurred, the polygon will **not** redraw itself to save resources. Polygons are created by default on the layer 'BelowPlayer'

To access the library's API, check out the [wiki page](https://gitlab.com/zavhartline/zvector/-/wikis/API) for more details.

---
## Examples
<img src="https://i.imgur.com/ae3Sjbz.gif" width=400>
<img src="https://i.imgur.com/gxJtklp.gif" width=400>

---
## Known Issues:

**1.)** The corners of acute angles are not filled in correctly. I have a solution in mind which involves masking, however I cannot guarantee the efficiency of it. As such, for now the acute corners will be janky.

---

## To Do List:

- [ ] Fix the corners of acute angles
- [ ] Optimize transformation resets
- [ ] Optimize rotation
- [ ] Test collision for more edge cases

---

