--[[
	Created by Zav Hartline
	https://gitlab.com/zavhartline
]]

MathUtils = {}

function MathUtils.PrintMatrix(u)
	for i=1, #u do
		DEBUG('{' .. table.concat(u[i], ", ") .. '}')
	end
end

function MathUtils.CloneMatrix(A)
	local result = {}
	for i=1, #A do
		result[i] = {}
		for j=1, #A[i] do
			result[i][j] =  A[i][j]
		end
	end
	return result
end

function MathUtils.ValidateMatrix(u)
	if type(u) ~= "table" then
		error('Expected row table, found ' .. type(u), 2)
	end
	local m = #u
	local n = #u[1]
	for i=1, m do
		if type(u[i]) ~= "table" then
			error('Expected column table, found ' .. type(u), 2)
		end
		if #u[i] ~= n then
			error('Matrix is not rectangular')
		end
		for j=1, n do
			if type(u[i][j]) ~= "number" then
				error('Expected number matrix member, found ' .. type(u[i][j]), 2)
			end
		end
	end
end

function MathUtils.DotProduct(u, v)
	if u == nil or v == nil then
		error('Attempt to find dot product of nil vector', 2)
	end
	if type(u) ~= "table" or type(v) ~= "table" then
		error('Attempt to find dot product of non-table value', 2)
	end
	if #u ~= #v then
		error('Attempt to find dot product of vectors with different dimensions', 2)
	end
	local sum = 0
	for i=1, #u do
		if type(u[i]) ~= "number" or type(v[i]) ~= "number" then
			error('Attempt to find dot product with non-numeric values ' .. type(u[i]) .. ' and ' .. type(v[i]), 2)
		end
		sum = sum + u[i] * v[i]
	end

	return sum
end

function MathUtils.Magnitude(u)
	if u == nil then
		error('Attempt to find magnitude of nil vector', 2)
	end
	if type(u) ~= "table" then
		error('Attempt to find magnitude of non-table value', 2)
	end
	local sum = 0
	for i=1, #u do
		if type(u[i]) ~= "number" then
			error('Attempt to find magnitude with non-numeric value of type ' .. type(u[i]), 2)
		end
		sum = sum + u[i] * u[i]
	end
	return math.sqrt(sum)
end

function MathUtils.MatrixEquality(u,v, precision)
	local status, msgout = pcall(MathUtils.ValidateMatrix, u)
	if status == false then error(msgout,2) end

	status, msgout = pcall(MathUtils.ValidateMatrix, v)
	if status == false then error(msgout,2) end

	precision = precision or 0.05
	for i=1, #u do
		for j=1, #u[1] do
			if math.abs(u[i][j] - v[i][j]) > precision then return false end
		end
	end
	return true
end	

function MathUtils.IsSquareMatrix(u)
	local status, msgout = pcall(MathUtils.ValidateMatrix,u)
	if status == false then error(msgout, 2) end

	local m = #u
	local n = #u[1]
	for i=1, m do
		if n ~= #u[i] then return false end
		if n ~= m then return false end
	end
	return true
end

function MathUtils.MatrixMultiplication(a, b)
	local status, msgout = pcall(MathUtils.ValidateMatrix, a)
	if status == false then error(msgout,2) end

	status, msgout = pcall(MathUtils.ValidateMatrix, b)
	if status == false then error(msgout,2) end

	if #a[1] ~= #b then
		error('Attempt to multiply matrices with non-matching dimensions', 2)
	end
	local c = {}
	local n = #a
	local m = #a[1]
	local p = #b[1]
	
	for i=1, n do
		c[i] = {}
		for j=1, p do
			local sum = 0
			for k=1, m do
				sum = sum + a[i][k] * b[k][j]
			end
			c[i][j] = sum
		end
	end
	return c
end

function MathUtils.SwapRows(u, a, b)
	local status, msgout = pcall(MathUtils.ValidateMatrix, u)
	if status == false then error(msgout,2) end

	local tmp = u[b]
	u[b] = u[a]
	u[a] = tmp
end

function MathUtils.GetREF(A)
	local status, msgout = pcall(MathUtils.ValidateMatrix, A)
	if status == false then error(msgout,2) end

	local h = 1
	local k = 1
	while h <= #A do
		local i_max = h
		for i=h, #A do
			if math.abs(A[i_max][k]) < math.abs(A[i][k]) then 
				i_max = i 
			end
		end
		if A[i_max][k] == 0 then
			k = k + 1
		else
			MathUtils.SwapRows(A, h, i_max)
			for i=h+1, #A do
				local f = A[i][k] / A[h][k]
				A[i][k] = 0
				for j=k+1, #A[1] do
					A[i][j] = A[i][j] - A[h][j] * f
				end
			end
			h = h+1
			k = k+1
		end
	end
end
function MathUtils.GetRREF(A)
	local status, msgout = pcall(MathUtils.ValidateMatrix, A)
	if status == false then error(msgout,2) end
	MathUtils.GetREF(A)
	local pivot = 1
	for i=1, #A do
		local pivotCoefficient = nil

		for j=pivot, #A[1] do
			if A[i][j] == 0 then 
				pivot = pivot + 1 
			elseif j == 1 or A[i][j-1] == 0 then
				break
			end
		end
		for j=#A[1], pivot, -1 do
			A[i][j] = A[i][j] / A[i][pivot]
			--Reduce columns
			for r=1, i-1 do
				A[r][j] = A[r][j] - A[r][pivot] * A[i][j]
			end
		end
	end

end
function MathUtils.Determinant(u)
	local status, msgout = pcall(MathUtils.ValidateMatrix,u)
	if status == false then error(msgout,2) end
	if not MathUtils.IsSquareMatrix(u) then
		error('Attempt to find determinant of non-square matrix', 2)
	end
	if #u == 2 then
		return u[1][1]*u[2][2] - u[1][2] * u[2][1]
	end
	if #u == 3 then
		--a(ei-fh)-b(di-fg)+c(dh-eg)
		return    u[1][1]*(u[2][2]*u[3][3]-u[2][3]*u[3][2])
				- u[1][2]*(u[2][1]*u[3][3]-u[2][3]*u[3][1])
				+ u[1][3]*(u[2][1]*u[3][2]-u[2][2]*u[3][1])
	end
	
	local tmp = MathUtils.CloneMatrix(u)
	MathUtils.GetREF(u)
	local product = 1
	for i=1, #u do
		product = product * u[i][i]
	end
	return product
end

function MathUtils.GetInverseMatrix(m)
	local status, msgout = pcall(MathUtils.ValidateMatrix, m)
	if status == false then error(msgout,2) end
	if MathUtils.IsSquareMatrix(m) == false then
		error('Attempt to find inverse of non-square matrix', 2)
	end
	if MathUtils.Determinant(m) == 0 then
		error('Attempt to find inverse of non-invertible matrix (determinant = 0)', 2)
	end
	if #m == 2 then
		local det = MathUtils.Determinant(m)
		return {{m[2][2]/det, -m[1][2]/det}, {-m[2][1]/det, m[1][1]/det}}
	end
	if #m == 3 then
		--3x3 matrix unrolled optimization
		local inv = {{},{},{}}
		local det = MathUtils.Determinant(m)
		inv[1][1] = MathUtils.Determinant({{m[2][2],m[2][3]},{m[3][2],m[3][3]}})/det
		inv[1][2] = MathUtils.Determinant({{m[1][3],m[1][2]},{m[3][3],m[3][2]}})/det
		inv[1][3] = MathUtils.Determinant({{m[1][2],m[1][3]},{m[2][2],m[2][3]}})/det
		inv[2][1] = MathUtils.Determinant({{m[2][3],m[2][1]},{m[3][3],m[3][1]}})/det
		inv[2][2] = MathUtils.Determinant({{m[1][1],m[1][3]},{m[3][1],m[3][3]}})/det
		inv[2][3] = MathUtils.Determinant({{m[1][3],m[1][1]},{m[2][3],m[2][1]}})/det
		inv[3][1] = MathUtils.Determinant({{m[2][1],m[2][2]},{m[3][1],m[3][2]}})/det
		inv[3][2] = MathUtils.Determinant({{m[1][2],m[1][1]},{m[3][2],m[3][1]}})/det
		inv[3][3] = MathUtils.Determinant({{m[1][1],m[1][2]},{m[2][1],m[2][2]}})/det
		return inv
	end

	local aug = MathUtils.CloneMatrix(m)
	for i=1, #aug do
		for j=1, #aug do
			if j == i then
				aug[i][#aug + i] = 1
			else
				aug[i][#aug + j] = 0
			end
		end
	end
	MathUtils.GetRREF(aug)
	local submatrix = {}
	for i=1, #aug do
		submatrix[i] = {}
		for j=1, #aug do
			submatrix[i][j] = aug[i][#aug+j]
		end
	end
	return submatrix
end

function MathUtils.VectorAngle(u, v)
	local theta = math.deg(math.acos(MathUtils.DotProduct(u, v) / (MathUtils.Magnitude(u) * MathUtils.Magnitude(v))))
	local correction = 1
	if v[2] < 0 then
		correction = -1
	end
	return correction * theta
end

return MathUtils
