--[[
	Created by Zav Hartline
	https://gitlab.com/zavhartline
]]
local Shape = {}
MathUtils = require((...):sub(1, (...):find("/Polygon$")) .. 'MatrixUtils')


function Shape.SetStroke(stroke)
	polygon.dirty = true
	polygon.stoke = stroke
end

function Shape.SetColor32(polygon, color)
	polygon.color = color
	for i=1, #polygon.sprites do
		if polygon.sprites[i].isactive then
			polygon.sprites[i].color32 = color
		end
	end
end

function Shape.SetAlpha32(polygon, a)
	for i=1, #polygon.sprites do
		if polygon.sprites[i].isactive then
			polygon.sprites[i].alpha32 = a
		end
	end
end

function Shape.SetLayer(polygon, layer)
	polygon.layer = layer
	for i=1, #polygon.sprites do
		if polygon.sprites[i].isactive then
			polygon.sprites[i].layer = layer
		end
	end
end

function Shape.SetOrigin(polygon, x, y)
	polygon.centerX = x
	polygon.centerY = y
end

function Shape.GetOrigin(polygon)
	return {polygon.centerX, polygon.centerY}
end

function Shape.Translate(polygon, x, y, isCached)
	if x == 0 and y == 0 then return end

	if isCached then
		Shape.Transform(polygon, {{1,0,x},{0,1,y},{0,0,1}})
		Shape.SetOrigin(polygon, polygon.centerX + x, polygon.centerY + y)
	else
		polygon.dirty = true
		for i=1, #polygon.verts do
			polygon.verts[i][1] = polygon.verts[i][1] + x
			polygon.verts[i][2] = polygon.verts[i][2] + y
		end
		Shape.SetOrigin(polygon, polygon.centerX + x, polygon.centerY + y)
	end
end

function Shape.SetTranslate(polygon, x, y)
	Shape.Translate(polygon, x - polygon.centerX, y - polygon.centerY)
end


function Shape.Rotate(polygon, deg)
	local rad = math.rad(deg)
	--if isCached then
		Shape.Transform(polygon, {{math.cos(rad),-math.sin(rad),0},{math.sin(rad), math.cos(rad),0},{0,0,1}})
	--[[else
		polygon.dirty = true
		for i=1, #polygon.verts do
			polygon.verts[i][1] = (polygon.verts[i][1]- polygon.centerX) * math.cos(rad) - (polygon.verts[i][2]-polygon.centerY) * math.sin(rad) + polygon.centerX
			polygon.verts[i][2] = (polygon.verts[i][2]-polygon.centerY) * math.cos(rad) + (polygon.verts[i][1]- polygon.centerX) * math.sin(rad) + polygon.centerY
		end
	end]]
end

function Shape.Transform(polygon, matrix)
	polygon.dirty = true
	polygon.currentMatrix = MathUtils.MatrixMultiplication(polygon.currentMatrix, matrix)

	for i=1, #polygon.verts do
		--Apply transformation matrix
		local vector = polygon.verts[i]
		local tmp = {vector[1] - polygon.centerX, vector[2] - polygon.centerY, 1}
		vector[1] = MathUtils.DotProduct(tmp, matrix[1]) + polygon.centerX
		vector[2] = MathUtils.DotProduct(tmp, matrix[2]) + polygon.centerY
	end
end

function Shape.ResetTransform(polygon)
	if polygon.dirty == false then return end
	local inverse = MathUtils.GetInverseMatrix(polygon.currentMatrix)
	Shape.Transform(polygon, inverse)
end

function Shape.SetTransform(polygon, tx)
	local tmp = polygon.currentMatrix
	Shape.ResetTransform(polygon)
	Shape.Transform(polygon, tx)
	polygon.dirty = true
end

function Shape.Destroy(polygon)
	for i=1,#polygon.sprites do
		polygon.sprites[i].Remove()
	end
	polygon.sprites = {}
end

local function DrawLine(polygon, current, nextVert, nextNextVert, spr)
	--Spawn 5x5 "pixel" of edge to turn into a line
	local edge = spr or CreateSprite('ZVectorSprites/geom_arena_edge', polygon.layer)
	edge.Scale(polygon.stroke * 0.2, polygon.stroke * 0.2)
	edge.x = current[1]
	edge.y = current[2]
	edge.xpivot = 0

	local v = {nextVert[1] - current[1], nextVert[2] - current[2]}
	local nextVector = {nextVert[1] - nextNextVert[1], nextVert[2] - nextNextVert[2]}
	local interior = MathUtils.VectorAngle(nextVector, v)
	local cornerCorrection = math.abs(math.tan(math.rad(((180-interior)/2))))
	local cornerpx = cornerCorrection * polygon.stroke
	local determinant = math.sqrt(MathUtils.DotProduct(v,v))
	local u = {determinant, 0}
	local theta = MathUtils.VectorAngle(u, v)
	local epsilon = 0.05
	if math.abs(interior) < 90 - epsilon then
		--TODO: Add corner filling for acute angles
		cornerpx = 0
	end
	--Fills in corners
	edge.Move(-math.cos(math.rad(theta)) * cornerpx / 2, -math.sin(math.rad(theta)) * cornerpx / 2)
	edge.color32 = polygon.color
	edge.rotation = theta
	edge.xscale = determinant / 5 + cornerpx / 5

	if spr == nil then table.insert(polygon.sprites, edge) end
end

function Shape.Update(polygon, shouldConnect)
	if polygon.verts == nil then return end
	if polygon.color == nil then polygon.color = {255,255,255} end
	if shouldConnect == nil then shouldConnect = true end
	if polygon.dirty == false then 
		return 
	end
	polygon.dirty = false

	for i=1,#polygon.verts do
		if shouldConnect == false and i == #polygon.verts then break end
		local current = polygon.verts[i]
		--Check if we need to connect to the start since we hit the last vertex
		local nextIndex = i + 1
		if i == #polygon.verts then 
			nextIndex = 1
		end
		local nextVert = polygon.verts[nextIndex]
		local nextNextVert = {polygon.verts[(i+1) % #polygon.verts + 1][1], polygon.verts[(i+1) % #polygon.verts + 1][2] }
		
		DrawLine(polygon, current, nextVert, nextNextVert, polygon.sprites[i])
	end
end

local Collision = {}
function Collision.OnSegment(p, q, r)
	if  q[1] <= math.max(p[1], r[1]) and
        q[1] >= math.min(p[1], r[1]) and
        q[2] <= math.max(p[2], r[2]) and
        q[2] >= math.min(p[2], r[2]) then
        return true
    end
    return false
end

function Collision.Orientation(p,q,r)
	local val = (q[2] - p[2]) * (r[1] - q[1]) - (q[1] - p[1]) * (r[2] - q[2])
	local epsilon = 0.05
	if math.abs(val) == epsilon then return 0 end
	if val > 0 then return 1 end
	return 2
end

function Collision.DoIntersect(p1, q1, p2, q2)
	local o1 = Collision.Orientation(p1, q1, p2)
    local o2 = Collision.Orientation(p1, q1, q2)
    local o3 = Collision.Orientation(p2, q2, p1)
    local o4 = Collision.Orientation(p2, q2, q1)
	if o1 ~= o2 and o3 ~= o4 then return true end

	if o1 == 0 and Collision.OnSegment(p1, p2, q1) then return true end
	if o2 == 0 and Collision.OnSegment(p1, q2, q1) then return true end
	if o3 == 0 and Collision.OnSegment(p2, p1, q2) then return true end
	if o4 == 0 and Collision.OnSegment(p2, q1, q2) then return true end
	return false
end

function Collision.IsInside(polygon, p)
	local n = #polygon.verts
	if n < 3 then return false end
	local extreme = {math.huge, p[2]}
	local count = 0
	local i = 1
	repeat
		local next = i%n +  1
		if Collision.DoIntersect(polygon.verts[i], polygon.verts[next], p, extreme) then
			if Collision.Orientation(polygon.verts[i], p, polygon.verts[next]) == 0 then
				return Collision.OnSegment(polygon.verts[i], p, polygon.verts[next])
			end
			count = count + 1
		end
		i = next
	until i == 1
	return count % 2 == 1
end

function Shape.CreateComplex(centerX, centerY, vertexList, stroke, color32)
	local polygon = {}
	polygon.centerX = centerX
	polygon.centerY = centerY
	polygon.sprites = {}
	polygon.layer = 'BelowPlayer'
	polygon.color = color32 or {255,255,255}
	polygon.currentMatrix = {{1,0,0}, {0,1,0},{0,0,1}}
	polygon.dirty = true
	polygon.verts = vertexList
	polygon.stroke = stroke or 5
	
	polygon.SetStroke = function(px) 		Shape.SetStroke(polygon, px) end
	polygon.SetColor32 = function(c) 		Shape.SetColor32(polygon, c) end
	polygon.SetAlpha32 = function(a) 		Shape.SetAlpha32(polygon, a) end
	polygon.SetLayer = function(l) 			Shape.SetLayer(polygon, l) end
	polygon.SetOrigin = function(x, y) 		Shape.SetOrigin(polygon, x, y) end
	polygon.GetOrigin = function() 			Shape.GetOrigin(polygon) end
	polygon.Translate = function(x, y, b) 	Shape.Translate(polygon, x, y, b) end
	polygon.SetTranslate = function(x, y) 	Shape.SetTranslate(polygon, x, y) end
	polygon.Rotate = function(d) 			Shape.Rotate(polygon, d) end
	polygon.Transform = function(m) 		Shape.Transform(polygon, m) end
	polygon.ResetTransform = function(m) 	Shape.ResetTransform(polygon) end
	polygon.SetTransform = function(m) 		Shape.SetTransform(polygon, m) end
	polygon.Destroy = function() 			Shape.Destroy(polygon) end
	polygon.Update = function(b)			return Shape.Update(polygon, b) end
	polygon.IsInside = function(p)			return Collision.IsInside(polygon, p) end
	return polygon
end

function Shape.CreateRegular(centerX, centerY, nSides, r, rotation, stroke, color32)
	if rotation == nil then rotation = 0 end
	local verts = {}
	for i=0,nSides-1 do
		local coords = {}
		coords[1] = centerX + r * math.cos(2 * math.pi * i / nSides + math.rad(rotation))
		coords[2] = centerY + r * math.sin(2 * math.pi * i / nSides + math.rad(rotation))
		table.insert(verts, coords)
	end

	return Shape.CreateComplex(centerX, centerY, verts, stroke, color32)
end

return Shape
