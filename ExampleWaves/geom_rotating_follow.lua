--[[
	Created by Zav Hartline
	https://gitlab.com/zavhartline
]]

local Polygon = require 'Libraries/ZVector/Polygon'
--Number of sides
local n = 4
--Difference in radius per iteration
local dr = 10
--Difference in starting angle
local dt = 15
--Width of each edge
local stroke = 5
--Stores shapes to update them
local shapes = {}


for i=1, 10 do
	--Generates ten different regular n-gons centered on the player 
	table.insert(shapes, Polygon.CreateRegular(Player.absx, Player.absy,n, 40 + dr * i, dt * i, stroke))
end

--Just to make the arena look nice
Arena.MoveAndResize(0,-85,620, 460, false)

function Update()
	for i=1, #shapes do

		--[[
			This conditional forces each shape to rotate a different direction than
			its neighbors
		]]
		if i % 2 == 0 then
			shapes[i].Rotate(1)
		else 
			shapes[i].Rotate(-1) 
		end
		--[[
			This translates each shape so that its position is centered at the player always
		]]
		shapes[i].SetTranslate(Player.absx, Player.absy)
		--[[
			Finally, force the shapes to recalculate their vertices
		]]
		shapes[i].Update()
	end
end