--[[
	Created by Zav Hartline
	https://gitlab.com/zavhartline
]]

local Polygon = require 'Libraries/ZVector/Polygon'

--Store all shapes so that we don't lose them
local shapes = {}

--Make the arena look pretty for painting
Arena.MoveAndResize(0,-85,620, 460, false)

--This just keeps track of input cooldown so you don't spam accidentally
local cooldown = 0

--This keeps track of the vertices we'll be generating for painting
local spritelist = {}

function Update()

	cooldown = cooldown - 1

	--If the input is Z, create a vertex at the player
	if Input.GetKey("Z") > 0 and cooldown < 0 then
		local tmp = CreateSprite('ZVectorSprites/geom_arena_edge', 'Top')
		tmp.MoveTo(Player.absx, Player.absy)
		table.insert(spritelist, tmp)
		cooldown = 20
	end

	--[[If the input is X, we'll create a complex polygon with all of the 
		positions previously logged by pressing Z.
	]]

	if Input.GetKey("X") > 0 and cooldown < 0 then
		local v = {}
		for i=1, #spritelist do
			v[i] = {spritelist[i].x, spritelist[i].y}
			spritelist[i].Remove()
		end
		spritelist = {}
		table.insert(shapes, Polygon.CreateComplex(0,0,v))
		cooldown = 20
	end

	--Just make sure everything is up to date
	for i=1, #shapes do
		shapes[i].Update()
	end
end